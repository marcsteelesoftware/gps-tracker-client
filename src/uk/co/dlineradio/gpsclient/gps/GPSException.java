package uk.co.dlineradio.gpsclient.gps;

/**
 * Exception for describing problems with the GPS system.
 * @author Marc Steele
 */

public class GPSException extends Exception {
	
	/**
	 * Creates a new instance of the exception.
	 * @param message The message to attach.
	 */
	
	public GPSException(String message) {
		super(message);
	}
	
	/**
	 * Creates a new instance of the exception.
	 * @param message The message to attach.
	 * @param innerException The inner exception (if there is one).
	 */
	
	public GPSException(String message, Exception innerException) {
		super(message, innerException);
	}

}
