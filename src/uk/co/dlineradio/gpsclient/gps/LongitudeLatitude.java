package uk.co.dlineradio.gpsclient.gps;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Longitude/latitude utility methods.
 * @author Marc Steele
 */

public class LongitudeLatitude {
	
	private static final Logger logger = Logger.getLogger(LongitudeLatitude.class.getName());
	private static final int SECONDS_PER_DEGREE = 60;
	
	/**
	 * Parse the longitude.
	 * @param value The raw value.
	 * @param eastWest The east/west value.
	 * @return The longitude if we could get it. Otherwise NULL.
	 */
	
	public static Double parseLongitude(String value, String eastWest) {
		
		// Sanity check
		
		if (value == null || value.isEmpty() || eastWest == null || eastWest.isEmpty()) {
			logger.log(Level.WARNING, "Could not parse the longitude value as we weren't supplied both the value and the east/west direction.");
			return null;
		}
		
		if (value.length() != 10) {
			logger.log(Level.WARNING, "Could not parse the longitude value as we weren't given a value of the right length.");
			return null;
		}
		
		// Parse the value
		
		try {
		
			double longitude = Double.parseDouble(value.substring(0, 3));
			double minutes = Double.parseDouble(value.substring(3));
			
			longitude += (minutes / 60);
			return eastWest.equalsIgnoreCase("W") ? longitude * -1.0d : longitude;
		
		} catch (Exception ex) {
			logger.log(Level.SEVERE, "Ran into a problem parsing the longitude value.", ex);
			return null;
		}
		
	}
	
	/**
	 * Parse the latitude.
	 * @param value The raw value.
	 * @param northSouth The north/south value.
	 * @return The latitude if we could get it. Otherwise NULL.
	 */
	
	public static Double parseLatitude(String value, String northSouth) {
		
		// Sanity check
		
		if (value == null || value.isEmpty() || northSouth == null || northSouth.isEmpty()) {
			logger.log(Level.WARNING, "Could not parse the latitude value as we weren't supplied both the value and the north/south direction.");
			return null;
		}
		
		if (value.length() != 9) {
			logger.log(Level.WARNING, "Could not parse the latitude value as we weren't given a value of the right length.");
			return null;
		}
		
		// Parse the value
		
		try {
		
			double latitude = Double.parseDouble(value.substring(0, 2));
			double minutes = Double.parseDouble(value.substring(2));
			
			latitude += (minutes / 60);
			return northSouth.equalsIgnoreCase("S") ? latitude * -1.0d : latitude;
		
		} catch (Exception ex) {
			logger.log(Level.SEVERE, "Ran into a problem parsing the latitude value.", ex);
			return null;
		}
		
	}

}
