package uk.co.dlineradio.gpsclient.gps;

import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.UnsupportedCommOperationException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The client for the GPS reciever.
 * @author Marc Steele
 */

public class GPSClient implements Runnable {
	
	private GPSStatus currentStatus = new GPSStatus();
	private Thread runningThread = null;
	private boolean runThread = false;
	private SerialPort serialPort = null;
	private BufferedReader serialInBuffered = null;
	
	private static final Logger logger = Logger.getLogger(GPSClient.class.getName());
	private static final int TIMEOUT_COM_PORT = 2000;
	private static final String PREFIX_GPS_FIX = "$GPRMC";
	private static final int COUNT_FIX_PARTS = 13;
	private static final int THREAD_SLEEP = 100;
	private static final int INDEX_STATUS = 2;
	private static final int INDEX_LATITUDE_VALUE = 3;
	private static final int INDEX_LATITUDE_NS = 4;
	private static final int INDEX_LONGITUDE_VALUE = 5;
	private static final int INDEX_LONGITUDE_EW = 6;
	private static final int INDEX_SPEED = 7;
	
	/**
	 * Make the user go round the long way. 
	 * We can force them to handle errors then. :)
	 * @throws GPSException Thrown if something went wrong with the process.
	 */
	
	private GPSClient(String comPort, int baud) throws GPSException {
		
		// Open the connection
		
		try {
			
			// Find the COM port and make sure it's not in use
			
			CommPortIdentifier comPortIdentifier = CommPortIdentifier.getPortIdentifier(comPort);
			if (comPortIdentifier.isCurrentlyOwned()) {
				throw new GPSException(String.format("Could not connect to the client at COM port %1$s is currently in use.", comPort));
			}
			
			// Open the input stream from the port
			
			CommPort selectedPort = comPortIdentifier.open(this.getClass().getName(), TIMEOUT_COM_PORT);
			if (selectedPort instanceof SerialPort) {
				
				// Setup the connection properties
				
				this.serialPort = (SerialPort) selectedPort;
				this.serialPort.setSerialPortParams(baud, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
				this.serialInBuffered = new BufferedReader(new InputStreamReader(this.serialPort.getInputStream()));
				
			} else {
				
				// Close down
				
				selectedPort.close();
				throw new GPSException(String.format("Could not connect to the GPS reciever at port %1$s is not a COM port.", comPort));
				
			}
			
		} catch (NoSuchPortException e) {
			String message = String.format("Could not connect to COM port %1$s as it doesn't exist.", comPort);
			logger.log(Level.SEVERE, message);
			throw new GPSException(message, e);
		} catch (PortInUseException e) {
			String message = String.format("Could not connect to COM port %1$s as it's in use.", comPort);
			logger.log(Level.SEVERE, message);
			throw new GPSException(message, e);
		} catch (UnsupportedCommOperationException e) {
			String message = String.format("Could not connect to COM port %1$s at baud rate %2$d as it's not supported.", comPort, baud);
			logger.log(Level.SEVERE, message);
			throw new GPSException(message, e);
		} catch (IOException e) {
			String message = String.format("Ran into an IO problem trying to connect to COM port %1$s.", comPort);
			logger.log(Level.SEVERE, message);
			throw new GPSException(message, e);
		}
		
		// Start the thread
		
		this.runThread = true;
		this.runningThread = new Thread(this);
		this.runningThread.start();
		
	}
	
	/**
	 * Creates a new instance of the client and connects it to a COM port.
	 * @param comPort The COM port to connect to.
	 * @param baud The baud rate we'll connect at.
	 * @return The new client if we were successful. Otherwise NULL.
	 */
	
	public static GPSClient get(String comPort, int baud) {
		
		// Sanity check
		
		if (comPort == null || comPort.isEmpty()) {
			logger.log(Level.SEVERE, "Could not start the client at no COM port was supplied.");
			return null;
		}
		
		if (baud <= 0) {
			logger.log(Level.SEVERE, String.format("Could not start the client on COM port %1$s at the supplied baud rate was invalid.", comPort));
			return null;
		}
		
		// Create the new client
		
		try {
			return new GPSClient(comPort, baud);
		} catch (GPSException e) {
			logger.log(Level.SEVERE, "Failed to setup the GPS reciever client.");
			return null;
		}
		
	}
	
	/**
	 * Obtains the current GPS status. This usually include location and speed.
	 * @return The current GPS status.
	 */

	public GPSStatus getCurrentStatus() {
		return currentStatus;
	}
	
	/**
	 * Disconnects from the GPS reciever.
	 */
	
	public void disconnect() {
		
		if (this.runThread && this.runningThread != null) {
			this.runThread = false;
			this.runningThread.interrupt();
		}
		
	}

	@Override
	public void run() {
		
		while (this.runThread) {
			
			try {
				
				// Read everything we can in one go
				
				while (this.serialInBuffered.ready()) {
					
					// Read in the line and make sure it starts the right way
					
					String currentLine = this.serialInBuffered.readLine();
					if (currentLine.startsWith(PREFIX_GPS_FIX)) {
						
						String[] fixParts = currentLine.split(",");
						if (fixParts != null && fixParts.length == COUNT_FIX_PARTS) {
							
							// Read in the parts
							
							boolean valid = fixParts[INDEX_STATUS].equals("A");
							Double latitude = LongitudeLatitude.parseLatitude(fixParts[INDEX_LATITUDE_VALUE], fixParts[INDEX_LATITUDE_NS]);
							Double longitude = LongitudeLatitude.parseLongitude(fixParts[INDEX_LONGITUDE_VALUE], fixParts[INDEX_LONGITUDE_EW]);
							double speed = Double.parseDouble(fixParts[INDEX_SPEED]);
							
							if (valid && latitude != null && longitude != null) {
								
								this.currentStatus.setLatitude(latitude);
								this.currentStatus.setLongitude(longitude);
								this.currentStatus.setValid(true);
								this.currentStatus.setLastFix(new Date());
								
							}
							
						}
						
						
					}
					
				}
				
			} catch (IOException e) {
				logger.log(Level.SEVERE, "Ran into an IO problem talking to the GPS reciever.", e);
			} catch (NumberFormatException e) {
				// Silently ignore - it means no data
			} catch (Exception e) {
				logger.log(Level.SEVERE, "Ran into a general problem talking to the GPS reciever.", e);
			}
			
			// Sleep it off
			
			try {
				Thread.sleep(THREAD_SLEEP);
			} catch (InterruptedException e) {
				logger.log(Level.WARNING, "GPS reciever client thread interrupted.", e);
			}
			
		}
		
		// Clean up
		
		try {
			
			this.serialInBuffered.close();
			this.serialPort.close();
			
		} catch (IOException e) {
			logger.log(Level.WARNING, "Ran into an IO problem disconnecting from the GPS reciever.", e);
		}
		
		
	}
	
}
