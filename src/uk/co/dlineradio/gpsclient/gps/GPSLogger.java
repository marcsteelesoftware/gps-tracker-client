package uk.co.dlineradio.gpsclient.gps;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import uk.co.dlineradio.gpsclient.repository.GPSTrackRecord;
import uk.co.dlineradio.gpsclient.repository.GPSTrackRepository;

/**
 * Logs GPS tracks to the database on a regular interval.
 * @author Marc Steele
 */

public class GPSLogger implements Runnable {
	
	private int interval;
	private Date lastSuccess = null;
	private GPSClient client = null;
	private GPSTrackRepository gpsRepo = null;
	private boolean runThread = false;
	private Thread runningThread = null;
	
	private static final int REFRESH_INTERVAL = 1000;
	private static Logger logger = Logger.getLogger(GPSLogger.class.getName());
	
	/**
	 * Creates a new instance of the logger.
	 * @param interval The interval between logs.
	 * @param client The GPS reciever client we'll use.
	 * @param gpsRepo The GPS track repository we'll be writing to.
	 */
	
	private GPSLogger(int interval, GPSClient client, GPSTrackRepository gpsRepo) {
		
		// Setup the basics
		
		this.interval = interval;
		this.client = client;
		this.gpsRepo = gpsRepo;
		
		// Start the thread
		
		this.runThread = true;
		this.runningThread = new Thread(this);
		this.runningThread.start();
		
	}
	
	/**
	 * Attempts to create a new instance of the logger.
	 * @param interval The interval between logs in milliseconds.
	 * @param client The GPS client we'll be listening to.
	 * @param gpsRepo The repository we'll be writing the tracks to.
	 * @return Assuming everything went well, you'll get a logger back. Otherwise, expect a NULL.
	 */
	
	public static GPSLogger get(int interval, GPSClient client, GPSTrackRepository gpsRepo) {
		
		// Sanity check
		
		if (interval <= 0) {
			logger.log(Level.WARNING, "Could not launch the logger as the interval wasn't valid!");
			return null;
		}
		
		if (client == null) {
			logger.log(Level.WARNING, "Could not launch the logger as the GPS client wasn't supplied.");
			return null;
		}
		
		if (gpsRepo == null) {
			logger.log(Level.WARNING, "Could not launch the logger as the database repository wasn't supplied.");
			return null;
		}
		
		// Go!
		
		return new GPSLogger(interval, client, gpsRepo);
		
	}

	@Override
	public void run() {
		
		while (this.runThread) {
			
			// Check if we need to log
			
			if (this.client.getCurrentStatus().isValid() && (this.lastSuccess == null || this.client.getCurrentStatus().getLastFix().getTime() - this.lastSuccess.getTime() > this.interval)) {
				
				// Log to the database
				
				GPSTrackRecord currentRecord = new GPSTrackRecord();
				currentRecord.setDateTime(this.client.getCurrentStatus().getLastFix());
				currentRecord.setLatitude(this.client.getCurrentStatus().getLatitude());
				currentRecord.setLongitude(this.client.getCurrentStatus().getLongitude());
				currentRecord.setSpeed(this.client.getCurrentStatus().getSpeed());
				
				if (this.gpsRepo.save(currentRecord)) {
					
					this.lastSuccess = currentRecord.getDateTime();
					logger.log(Level.INFO, String.format("Successfully logged %1$s.", currentRecord));
					
				} else {
					
					logger.log(Level.SEVERE, String.format("Failed to log %1$s.", currentRecord));
					
				}
				
			}
			
			// Sleep it off
			
			try {
				Thread.sleep(REFRESH_INTERVAL);
			} catch (InterruptedException e) {
				logger.log(Level.WARNING, "GPS logging thread interrupted.");
			}
			
		}
		
	}
	
	/**
	 * Stops the running thread.
	 */
	
	public void stopThread() {
		
		if (this.runThread && this.runningThread != null) {
			this.runThread = false;
			this.runningThread.interrupt();
			this.runningThread = null;
		}
		
	}

}
