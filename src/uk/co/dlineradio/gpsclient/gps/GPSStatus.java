package uk.co.dlineradio.gpsclient.gps;

import java.util.Date;

/**
 * Holds the current status of the GPS fix.
 * @author Marc Steele
 *
 */

public class GPSStatus {
	
	private double latitude;
	private double longitude;
	private double speed;
	private boolean valid = false;
	private Date lastFix = null;
	
	/**
	 * Obtains the current latitude.
	 * @return The current latitude.
	 */
	
	public double getLatitude() {
		return latitude;
	}
	
	/**
	 * Sets the current latitude.
	 * @param latitude The current latitude.
	 */
	
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	
	/**
	 * Obtains the current longitude.
	 * @return The current longitude.
	 */
	
	public double getLongitude() {
		return longitude;
	}
	
	/**
	 * Sets the current longitude.
	 * @param longitude The current longitude.
	 */
	
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	/**
	 * Obtains the current speed.
	 * @return The current speed in knots.
	 */
	
	public double getSpeed() {
		return speed;
	}
	
	/**
	 * Sets the current speed.
	 * @param speed The current speed in knots.
	 */
	
	public void setSpeed(double speed) {
		this.speed = speed;
	}
	
	/**
	 * Flags if the current reading is valid. 
	 * @param valid TRUE if the current reading is valid.
	 */
	
	public void setValid(boolean valid) {
		this.valid = valid;
	}
	
	/**
	 * Indicates if the current reading is valid.
	 * @return TRUE if it's valid.
	 */
	
	public boolean isValid() {
		return valid;
	}

	public String toString() {
		
		if (this.isValid()) {
			return String.format("%1$f,%2$f moving at %3$f knots", this.latitude, this.longitude, this.speed);
		} else {
			return "No valid fix.";
		}
		
	}
	
	/**
	 * Obtains the date/time the last successful fix was made.
	 * @return The date/time.
	 */

	public Date getLastFix() {
		return lastFix;
	}
	
	/**
	 * Sets the date/time the last successful fix was made.
	 * @param lastFix
	 */

	public void setLastFix(Date lastFix) {
		this.lastFix = lastFix;
	}
	
}
