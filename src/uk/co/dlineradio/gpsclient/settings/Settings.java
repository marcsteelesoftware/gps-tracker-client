package uk.co.dlineradio.gpsclient.settings;

/**
 * Holds the application settings.
 * @author Marc Steele
 */

public class Settings {
	
	private String gpsComPort;
	private int gpsSerialBaud;
	private String logDBFile;
	private int gpsTrackInterval;
	private int logInterval;
	private String logServer;
	private String guid;
	private String key;
	
	/**
	 * Obtains the interval between GPS tracks.
	 * @return The interval in milliseconds.
	 */
	
	public int getGpsTrackInterval() {
		return gpsTrackInterval;
	}
	
	/**
	 * Sets the interval between GPS tracks.
	 * @param gpsTrackInterval The interval in milliseconds.
	 */

	public void setGpsTrackInterval(int gpsTrackInterval) {
		this.gpsTrackInterval = gpsTrackInterval;
	}

	/**
	 * Obtains the COM port the GPS reciever is on.
	 * @return The COM port the GPS reciever is on.
	 */
	
	public String getGpsComPort() {
		return gpsComPort;
	}
	
	/**
	 * Sets the COM port the GPS reciever is on.
	 * @param gpsComPort The COM port the GPS reciever is on.
	 */
	
	public void setGpsComPort(String gpsComPort) {
		this.gpsComPort = gpsComPort;
	}
	
	/**
	 * Obtains the serial baud rate the GPS reciever communicates at.
	 * @return The serial baud rate the GPS reciever communicates at.
	 */
	
	public int getGpsSerialBaud() {
		return gpsSerialBaud;
	}
	
	/**
	 * Sets the serial baud rate the GPS reciever communicates at.
	 * @param gpsSerialBaud The serial baud rate the GPS reciever communicates at.
	 */
	
	public void setGpsSerialBaud(int gpsSerialBaud) {
		this.gpsSerialBaud = gpsSerialBaud;
	}
	
	/**
	 * Obtains the name of the database file we'll log the tracks into.
	 * @return The name of the database file.
	 */

	public String getLogDBFile() {
		return logDBFile;
	}
	
	/**
	 * Sets the name of the database file we'll log the tracks into.
	 * @param logDBFile The name of the database file.
	 */

	public void setLogDBFile(String logDBFile) {
		this.logDBFile = logDBFile;
	}
	
	/**
	 * Obtains the interval between logging attempts.
	 * @return The interval in milliseconds.
	 */

	public int getLogInterval() {
		return logInterval;
	}
	
	/**
	 * Sets the interval between logging attempts.
	 * @param logInterval The interval in milliseconds.
	 */

	public void setLogInterval(int logInterval) {
		this.logInterval = logInterval;
	}
	
	/**
	 * Obtains the server we're logging data to.
	 * @return The server.
	 */

	public String getLogServer() {
		return logServer;
	}
	
	/**
	 * Sets the server we're logging data to.
	 * @param logServer The server.
	 */

	public void setLogServer(String logServer) {
		this.logServer = logServer;
	}
	
	/**
	 * Obtains the GUID of the device.
	 * @return The GUID of the device.
 	 */

	public String getGuid() {
		return guid;
	}

	/**
	 * Sets the GUID of the device.
	 * @param guid The GUID of the device.
	 */
	
	public void setGuid(String guid) {
		this.guid = guid;
	}
	
	/**
	 * Obtains the secret key for the device.
	 * @return The secret key for the device.
	 */

	public String getKey() {
		return key;
	}

	/**
	 * Sets the secret key for the device.
	 * @param key The secret key for the device.
	 */
	
	public void setKey(String key) {
		this.key = key;
	}

}
