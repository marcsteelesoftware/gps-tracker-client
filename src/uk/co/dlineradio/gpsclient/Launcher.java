package uk.co.dlineradio.gpsclient;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.cli.CommandLine;

import uk.co.dlineradio.gpsclient.cli.CommandLineParser;
import uk.co.dlineradio.gpsclient.gps.GPSClient;
import uk.co.dlineradio.gpsclient.gps.GPSLogger;
import uk.co.dlineradio.gpsclient.logging.HttpLogger;
import uk.co.dlineradio.gpsclient.repository.GPSTrackRepository;
import uk.co.dlineradio.gpsclient.settings.Settings;
import uk.co.dlineradio.gpsclient.util.XMLSerialiser;

/**
 * Launchers the GPS client application.
 * @author Marc Steele
 */

public class Launcher {
	
	private static final Logger logger = Logger.getLogger(Launcher.class.getName());

	/**
	 * Launches the client.
	 * @param args The command line arguments.
	 */
	
	public static void main(String[] args) {
		
		// Load the settings
		
		CommandLine commandLine = CommandLineParser.parse(args);
		Settings settings = (Settings) XMLSerialiser.get().getFromFile(commandLine.getOptionValue("settings"));
		if (settings == null) {
			logger.log(Level.SEVERE, String.format("Failed to load settings from %1$s. Bailing out.", commandLine.getOptionValues("settings")));
			System.exit(0);
		}
		
		// Launch the GPS client
		
		GPSClient gpsClient = GPSClient.get(settings.getGpsComPort(), settings.getGpsSerialBaud());

		// Setup the database for the other parts of the application
		
		GPSTrackRepository gpsRepo = new GPSTrackRepository(settings.getLogDBFile());
		if (!gpsRepo.connect() || !gpsRepo.initialseDatabase()) {
			logger.log(Level.SEVERE, "Could not connect to and initialise the database. Bailing out.");
			System.exit(0);
		}
		
		// Start the logger
		
		GPSLogger gpsLogger = GPSLogger.get(settings.getGpsTrackInterval(), gpsClient, gpsRepo);
		if (gpsLogger == null) {
			logger.log(Level.SEVERE, "Could not launch the logger. Bailing out.");
			System.exit(0);
		}
		
		// And now the logger back to the server
		
		HttpLogger httpLogger = HttpLogger.get(settings.getLogServer(), settings.getLogInterval(), gpsRepo, settings.getGuid(), settings.getKey());
		if (httpLogger == null) {
			logger.log(Level.SEVERE, "Could not launch the compontent for sending data back to the server. Bailing out.");
			System.exit(0);
		}
		
	}
	

}
