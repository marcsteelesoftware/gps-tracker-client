package uk.co.dlineradio.gpsclient.logging;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;

import uk.co.dlineradio.gpsclient.repository.GPSTrackRecord;
import uk.co.dlineradio.gpsclient.repository.GPSTrackRepository;
import uk.co.dlineradio.gpsclient.util.HTTPUtils;
import uk.co.dlineradio.gpsclient.util.HttpUtilResponse;

/**
 * Logs GPS tracks to an HTTP server. Specifically Pi Hunter.
 * @author Marc Steele
 */

public class HttpLogger implements Runnable {
	
	private String server;
	private int interval;
	private String guid;
	private String key;
	private GPSTrackRepository trackRepo;
	private boolean runThread = false;
	private Thread runningThread;
	
	private static Logger logger = Logger.getLogger(HttpLogger.class.getName());
	
	/**
	 * Creates a new instance of the logger.
	 * @param server The server we're logging to.
	 * @param interval The interval between checks.
	 * @param guid The GUID of the device we're tracking.
	 * @param key The private key of the device we're tracking.
	 * @param trackRepo The repository we're pulling the tracks from.
	 */
	
	private HttpLogger(String server, int interval, GPSTrackRepository trackRepo, String guid, String key) { 
		
		// Assignments
		
		this.server = server;
		this.interval = interval;
		this.trackRepo = trackRepo;
		this.guid = guid;
		this.key = key;
		
		// Launch the thread
		
		logger.log(Level.INFO, String.format("Launching the logger for the server %1$s.", this.server));
		this.runThread = true;
		this.runningThread = new Thread(this);
		this.runningThread.start();
		
	}
	
	/**
	 * Attempts to create a new instance of the logger.
	 * @param server The server we're logging to.
	 * @param interval The interval between checks.
	 * @param guid The GUID of the device we're tracking.
	 * @param key The private key of the device we're tracking.
	 * @param trackRepo The repository we're pulling the tracks from.
	 * @return The logger if we could generate it. NULL if something went wrong.
	 */
	
	public static HttpLogger get(String server, int interval, GPSTrackRepository trackRepo, String guid, String key) {
		
		// Sanity check
		
		if (server == null || server.isEmpty()) {
			logger.log(Level.WARNING, "Could not launch the logger as no server was supplied.");
			return null;
		}
		
		if (interval <= 0) {
			logger.log(Level.WARNING, "Could not launch the logger as an invalid interval time was given.");
			return null;
		}
		
		if (trackRepo == null) {
			logger.log(Level.WARNING, "Could not launch the logger as we weren't supplied a repository to log the entries to.");
			return null;
		}
		
		if (guid == null || guid.isEmpty()) {
			logger.log(Level.WARNING, "Could not launch the logger as the GUID of the device was not supplied.");
			return null;
		}
		
		if (key == null || key.isEmpty()) {
			logger.log(Level.WARNING, "Could not launch the logger as the key for the device as not supplied.");
			return null;
		}
		
		// Generate the new logger
		
		return new HttpLogger(server, interval, trackRepo, guid, key);
		
	}

	@Override
	public void run() {
		
		while (this.runThread) {
			
			// Pull in any tracks we've not logged yet
			
			List<GPSTrackRecord> tracks = this.trackRepo.getAll();
			if (tracks != null) {
				
				for (GPSTrackRecord currentTrack:tracks) {
					
					// Attempt to log the track
					
					if (this.logTrack(currentTrack)) {
						this.trackRepo.delete(currentTrack);
						logger.log(Level.INFO, String.format("Successfully logged track for %1$s to the server.", currentTrack.getDateTime()));
					} else {
						logger.log(Level.INFO, String.format("Ran into a problem logging the track for %1$s to the server.", currentTrack.getDateTime()));
					}
					
				}
				
			} else {
				logger.log(Level.WARNING, "Weird... we couldn't retrieve any GPS tracks.");
			}
			
			// Sleep it off
			
			try {
				Thread.sleep(this.interval);
			} catch (InterruptedException e) {
				logger.log(Level.WARNING, "HTTP logger thread interrupted.");
			}
			
		}
		
	}
	
	/**
	 * Stops the running thread.
	 */
	
	public void stopThread() {
		
		if (this.runThread && this.runningThread != null) {
			this.runThread = false;
			this.runningThread.interrupt();
		}
		
	}
	
	/**
	 * Attempts to log a specific track to the server.
	 * @param track The track we wish to log.
	 * @return TRUE if the process was successful. Otherwise FALSE.
	 */
	
	private boolean logTrack(GPSTrackRecord track) {
		
		// Sanity check
		
		if (track == null) {
			logger.log(Level.WARNING, "Asked to log a track to the server but we were not supplied the track!");
			return false;
		}
		
		// Build the URL
		// Start with the parameters
		
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("guid", this.guid);
		parameters.put("timestamp", String.format("%1$d", track.getDateTime().getTime() / 1000));
		parameters.put("latitude", String.format("%1$f", track.getLatitude()));
		parameters.put("longitude", String.format("%1$f", track.getLongitude()));
		parameters.put("speed", String.format("%1$f", track.getSpeed()));
		parameters.put("sig", this.generateSignature(parameters));
		
		// Now kerplunk the whole URL together
		
		int count = 0;
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("http://%1$s/devices/track/?", this.server));
		
		for (String currentKey:parameters.keySet()) {
			
			if (count == 0) {
				sb.append(String.format("%1$s=%2$s", currentKey, parameters.get(currentKey)));
			} else {
				sb.append(String.format("&%1$s=%2$s", currentKey, parameters.get(currentKey)));
			}
			
			count++;
			
		}
		
		String url = sb.toString();
		
		// Now make and parse the request
		
		HttpUtilResponse response = HTTPUtils.get(url);
		
		if (response != null) {
			if (response.getCode() == 200) {
				
				// Check the content of the response
				
				if (response.getContent() != null && response.getContent().startsWith("OK")) {
					logger.log(Level.INFO, String.format("Successfully logged track for %1$s.", track.getDateTime()));
					return true;
				} else {
					logger.log(Level.SEVERE, String.format("Ran into a problem logging the track. Response: %1$s", response.getContent()));
					return false;
				}
				
			} else {
				logger.log(Level.SEVERE, String.format("Failed to log track. HTTP code %1%d.", response.getCode()));
				return false;
			}
			
		} else {
			logger.log(Level.SEVERE, "Failed to get a response from the track logging request.");
			return false;
		}
		
	}
	
	/**
	 * Generates the signature for the HTTP request.
	 * @param parameters The parameters of the request.
	 * @return The signature (if we could generate one).
	 */
	
	private String generateSignature(Map<String, String> parameters) {
		
		// Sanity check
		
		if (parameters == null || parameters.isEmpty()) {
			logger.log(Level.WARNING, "Could not generate the HTTP request signature as we were not supplied the parameters of the request.");
			return null;
		}
		
		// Generate the signature text
		
		Set<String> paramNamesUnsorted = parameters.keySet();
		List<String> paramNames = new ArrayList<String>(paramNamesUnsorted);
		Collections.sort(paramNames);
		
		StringBuilder sb = new StringBuilder();
		int count = 0;
		
		for (String currentParamName:paramNames) {
			
			if (count == 0) {
				sb.append(String.format("%1$s=%2$s", currentParamName, parameters.get(currentParamName)));
			} else {
				sb.append(String.format("&%1$s=%2$s", currentParamName, parameters.get(currentParamName)));
			}
			
			count++;
			
		}
		
		// Now the signature itself
		
		try {
			
			String originalText = sb.toString();
			Mac sha256 = Mac.getInstance("HmacSHA256");
			SecretKeySpec secretKey = new SecretKeySpec(this.key.getBytes(), "HmacSHA256");
			sha256.init(secretKey);
			return Hex.encodeHexString(sha256.doFinal(originalText.getBytes()));
			
		} catch (NoSuchAlgorithmException e) {
			logger.log(Level.SEVERE, "Turns out the algorithm we need to generate the signature is nowhere to be seen. That's not good.", e);
			return null;
		} catch (InvalidKeyException e) {
			logger.log(Level.SEVERE, "Turns out the key we've got for the device is invalid.", e);
			return null;
		}
		
		
	}

}
