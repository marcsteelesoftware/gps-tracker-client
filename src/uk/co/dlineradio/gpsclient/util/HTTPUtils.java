package uk.co.dlineradio.gpsclient.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

/**
 * HTTP utility methods.
 * @author Marc Steele
 */

public class HTTPUtils {
	
	private static Logger logger = Logger.getLogger(HTTPUtils.class.getName());
	
	/**
	 * Performs an HTTP GET request.
	 * @param url The URL to get.
	 * @return The response from the server if all went well. Worst case you'll
	 * get a null.
	 */
	
	public static HttpUtilResponse get(String url) {
		
		// Sanity check
		
		if (url == null || url.isEmpty()) {
			logger.log(Level.WARNING, "Could not perform the HTTP GET request as no URL was supplied.");
			return null;
		}
		
		// Perform the request
		
		try {
			
			HttpClient client = new DefaultHttpClient();
			HttpGet getRequest = new HttpGet(url);
			HttpResponse response = client.execute(getRequest);
			
			// And process the response
			
			HttpUtilResponse responseWrapper = new HttpUtilResponse();
			responseWrapper.setCode(response.getStatusLine().getStatusCode());
			
			HttpEntity entity = response.getEntity();
			if (entity != null) {
				responseWrapper.setContent(EntityUtils.toString(entity));
			}
			
			return responseWrapper;
			
		} catch (ClientProtocolException e) {
			logger.log(Level.SEVERE, String.format("Ran into a protocol problem trying to perform an HTTP GET to %1$s.", url), e);
			return null;
		} catch (IOException e) {
			logger.log(Level.SEVERE, String.format("Ran into an IO problem trying to perform an HTTP GET to %1$s.", url), e);
			return null;
		}
		
	}

}
