package uk.co.dlineradio.gpsclient.util;

/**
 * Represents a response from a HTTP request.
 * @author Marc Steele
 */

public class HttpUtilResponse {
	
	private String content;
	private int code;
	
	/**
	 * Obtains the string content of the response.
	 * @return The string content of the response.
	 */
	
	public String getContent() {
		return content;
	}
	
	/**
	 * Sets the string content of the response.
	 * @param content The string content of the response.
	 */
	
	public void setContent(String content) {
		this.content = content;
	}
	
	/**
	 * Obtains the HTTP response code.
	 * @return The HTTP response code.
	 */
	
	public int getCode() {
		return code;
	}
	
	/**
	 * Sets the HTTP response code.
	 * @param code The response code.
	 */
	
	public void setCode(int code) {
		this.code = code;
	}

}
