package uk.co.dlineradio.gpsclient.repository;

import java.util.Date;

/**
 * Represents a record of a GPS track in the database.
 * @author Marc Steele
 */

public class GPSTrackRecord {
	
	private Date dateTime;
	private double latitude;
	private double longitude;
	private double speed;
	
	/**
	 * Obtains the speed when the track occurred.
	 * @return The speed in knots.
	 */
	
	public double getSpeed() {
		return speed;
	}
	
	/**
	 * Sets the speed when the track occurred.
	 * @param speed The speed in knots.
	 */
	
	public void setSpeed(double speed) {
		this.speed = speed;
	}
	
	/**
	 * Obtains the date and time the track occurred.
	 * @return The date and time.
	 */
	
	public Date getDateTime() {
		return dateTime;
	}
	
	/**
	 * Sets the date and time the track occurred.
	 * @param dateTime The date and time.
	 */
	
	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}
	
	/**
	 * Obtains the latitude of the track.
	 * @return The latitude of the track.
	 */
	
	public double getLatitude() {
		return latitude;
	}
	
	/**
	 * Sets the latitude of the track.
	 * @param latitude The latitude of the track.
	 */
	
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	
	/**
	 * Obtains the longitude of the track.
	 * @return The longitude of the track.
	 */
	
	public double getLongitude() {
		return longitude;
	}
	
	/**
	 * Sets the longitude of the track.
	 * @param longitude The longitude of the track.
	 */
	
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	@Override
	public String toString() {
		return String.format("%1$f,%2$f going %3$f knots at %4$s", this.latitude, this.longitude, this.speed, this.dateTime);
	}

}
