package uk.co.dlineradio.gpsclient.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Provides access to the local GPS track database.
 * @author Marc Steele
 */

public class GPSTrackRepository {

	private String dbFileName = null;
	private Connection connection = null;
	
	private static Logger logger = Logger.getLogger(GPSTrackRepository.class.getName());
	
	/**
	 * Loads the SQLite drivers.
	 */
	
	static {
		
		try {
			Class.forName("org.sqlite.JDBC");
		} catch (ClassNotFoundException e) {
			logger.log(Level.SEVERE, "Failed to load SQLite driver. This isn't going to end well.");
		}
	}
	
	/**
	 * Creates a new instance of the repository.
	 * @param dbFileName The name of the database file.
	 */
	
	public GPSTrackRepository(String dbFileName) {
		this.dbFileName = dbFileName;
	}
	
	/**
	 * Connects to the database.
	 * @return TRUE if we connected successfully. Otherwise FALSE.
	 */
	
	public boolean connect() {
		
		// Sanity check
		
		if (this.dbFileName == null || this.dbFileName.isEmpty()) {
			logger.log(Level.SEVERE, "Could not connect to the SQLite database as no filename was supplied.");
			return false;
		}
		
		// Create the connection
		
		try {
			
			this.connection = DriverManager.getConnection(String.format("jdbc:sqlite:%1$s", this.dbFileName));
			return true;
			
		} catch (SQLException e) {
			logger.log(Level.SEVERE, String.format("Ran into a problem connecting to the SQLite database at %1$s.", this.dbFileName), e);
			return false;
		}
		
	}
	
	/**
	 * Disconnects from the database.
	 */
	
	public void disconnect() {
		
		if (this.isConnected()) {
			
			// Attempt to disconnect
			
			try {
				this.connection.close();
			} catch (SQLException e) {
				logger.log(Level.WARNING, String.format("Failed to disconnect from the SQLite database at %1$s.", this.dbFileName));
			}
			
			// Hide the connection
			
			this.connection = null;
			
		}
		
	}
	
	/**
	 * Indicates if we're connected to the database.
	 * @return TRUE if we are. FALSE if we're not.
	 */
	
	private boolean isConnected() {
		return this.connection != null;
	}
	
	/**
	 * Attempts to initialise the database.
	 * @return TRUE if the process was successful. Otherwise FALSE.
	 */
	
	public boolean initialseDatabase() {
		
		// Check we have a connection
		
		if (!this.isConnected()) {
			logger.log(Level.SEVERE, "Could not initialise database as we're not connected!");
			return false;
		}
		
		// Create the log table as required
		
		try {
			
			Statement statement = this.connection.createStatement();
			statement.executeUpdate("CREATE TABLE IF NOT EXISTS gpslog (timestamp TEXT, latitude REAL, longitude REAL, speed REAL)");
			statement.close();
			return true;
			
		} catch (SQLException e) {
			logger.log(Level.SEVERE, "Something went wrong initialising the database.", e);
			return false;
		}
		
	}
	
	/**
	 * Saves the specified GPS track record to the database.
	 * @param record The record to save.
	 * @return TRUE if the process was successful. Otherwise FALSE.
	 */
	
	public boolean save(GPSTrackRecord record) {
		
		// Sanity check
		
		if (record == null) {
			logger.log(Level.WARNING, "Could not save the GPS track record to the database as no record was supplied.");
			return false;
		}
		
		if (record.getDateTime() == null) {
			logger.log(Level.WARNING, "Could not save the GPS track record as it has no date/time associated with it.");
			return false;
		}
		
		// Make sure we can connect
		
		if (!this.isConnected()) {
			logger.log(Level.SEVERE, "Could not save the GPS track record to the database as we're not connected!");
			return false;
		}
		
		// Perform the save
		
		try {
			
			PreparedStatement statement = this.connection.prepareStatement("INSERT INTO gpslog (timestamp, latitude, longitude, speed) VALUES (?, ?, ?, ?)");
			statement.setDate(1, new Date(record.getDateTime().getTime()));
			statement.setDouble(2, record.getLatitude());
			statement.setDouble(3, record.getLongitude());
			statement.setDouble(4, record.getSpeed());
			
			statement.execute();
			statement.close();
			return true;
			
		} catch (SQLException e) {
			logger.log(Level.SEVERE, String.format("Ran into a problem saving the following record to the database: %1$s.", record), e);
			return false;
		}
		
	}
	
	/**
	 * Retrieves all the GPS track records from the database.
	 * @return The list of records.
	 */
	
	public List<GPSTrackRecord> getAll() {
		
		// Make sure we can connect
		
		if (!this.isConnected()) {
			logger.log(Level.SEVERE, "Could not retrieve the GPS track records from the database as we're not connected!");
			return null;
		}
		
		// Perform the retrieval
		
		try {
			
			Statement statement = this.connection.createStatement();
			ResultSet rs = statement.executeQuery("SELECT * FROM gpslog");
			
			List<GPSTrackRecord> results = new LinkedList<GPSTrackRecord>();
			while (rs.next()) {
				results.add(this.inflateRecord(rs));
			}
			
			rs.close();
			statement.close();
			return results;
			
		} catch (SQLException ex) {
			logger.log(Level.SEVERE, "Ran into a problem trying to retrieve all the GPS track records.", ex);
			return null;
		}
		
	}
	
	/**
	 * Inflates the GPS track record.
	 * @param rs The result set we're inflating from.
	 * @return The inflated record.
	 * @throws SQLException Something went wrong with the process.
	 */
	
	private GPSTrackRecord inflateRecord(ResultSet rs) throws SQLException {
		
		// Sanity check
		
		if (rs == null) {
			logger.log(Level.SEVERE, "Failed to inflate the GPS track record as we weren't supplied a result set.");
			return null;
		}
		
		// Inflate
		
		GPSTrackRecord currentRecord = new GPSTrackRecord();
		currentRecord.setDateTime(rs.getDate("timestamp"));
		currentRecord.setLatitude(rs.getDouble("latitude"));
		currentRecord.setLongitude(rs.getDouble("longitude"));
		currentRecord.setSpeed(rs.getDouble("speed"));
		return currentRecord;
		
	}
	
	/**
	 * Attempts to delete the supplied record from the database.
	 * @param record The record to delete.
	 * @return TRUE if the process was successful.
	 */
	
	public boolean delete(GPSTrackRecord record) {
		
		// Sanity check
		
		if (record == null) {
			logger.log(Level.SEVERE, "Could not delete the GPS track record as we weren't told which one.");
			return false;
		}
		
		if (record.getDateTime() == null) {
			logger.log(Level.SEVERE, "Could not delete the GPS track record as we weren't supplied the date/time for the record.");
			return false;
		}
		
		// Make sure we can connect
		
		if (!this.isConnected()) {
			logger.log(Level.SEVERE, "Could not delete the GPS track record from the database as we're not connected!");
			return false;
		}
		
		// Perform the deletion
		
		try {
			
			PreparedStatement statement = this.connection.prepareStatement("DELETE FROM gpslog WHERE timestamp = ?");
			statement.setDate(1, new Date(record.getDateTime().getTime()));
			statement.execute();
			statement.close();
			return true;
			
		} catch (SQLException ex) {
			logger.log(Level.SEVERE, String.format("Could not delete the GPS track record at %1$s at we ran into an SQL problem.", record.getDateTime()), ex);
			return false;
		}
		
	}
	
}
