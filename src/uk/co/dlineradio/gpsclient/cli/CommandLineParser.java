package uk.co.dlineradio.gpsclient.cli;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;

/**
 * Parses the command line settings for the application.
 * @author Marc Steele
 */

public class CommandLineParser {
	
	private static Logger logger = Logger.getLogger(CommandLineParser.class.getName());
	
	/**
	 * Parses the command line.
	 * @param args The command line arguments.
	 * @return The parsed command line.
	 */
	
	public static CommandLine parse(String[] args) {
		
		// Setup the options
		
		Options options = new Options();
		options.addOption(
				OptionBuilder
					.withArgName("file")
					.hasArg()
					.withDescription("The settings file name")
					.create("settings"));
		
		// Parse the command line
		
		try {
			
			PosixParser parser = new PosixParser();
			CommandLine commandLine = parser.parse(options, args);
			
			if (commandLine.hasOption("settings")) {
				return commandLine;
			} else {
				showHelp(options);
				return null;
			}
			
		} catch (ParseException e) {
			logger.log(Level.SEVERE, "Ran into a problem parsing the command line options.", e);
			showHelp(options);
			return null;
		}
		
	}
	
	/**
	 * Shows the help page.
	 * @param options The command line options.
	 */
	
	public static void showHelp(Options options) {
		
		HelpFormatter helpFormatter = new HelpFormatter();
		helpFormatter.printHelp("GPS Tracker Client", options);
		System.exit(0);
		
	}

}
